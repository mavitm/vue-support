import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
    userid: 0,
    usename: "",
    message: "",
    messageList: [],
    reciveMessage:[]
};
const getters = {};

const mutations = {
    insertMessage(state, item) {
        state.messageList.push(item);
    },
    SOCKET_reciveMessage(state, reciveMessage)
    {
        state.reciveMessage = reciveMessage;
    }
};

const actions = {
    appendMessage({commit}, item){
        commit('insertMessage', item);
    },
    SOCKET_setRemoteMessage({commit}, reciveMessage){
        commit('SOCKET_reciveMessage', reciveMessage);
        commit('insertMessage', reciveMessage);
    }
};

const store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions,
});

export default store;