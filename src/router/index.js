import Vue from 'vue'
import Router from 'vue-router'
import SupportMain from '@/components/SupportMain'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'SupportMain',
            component: SupportMain
        }
    ]
})