import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import store from './store';
import VueSocketIO from 'vue-socket.io'

Vue.config.productionTip = false
Vue.use(BootstrapVue)

Vue.use(new VueSocketIO({
    debug: true,
    connection: 'http://127.0.0.1:3000',
    vuex: {
        store,
        actionPrefix: 'SOCKET_',
        mutationPrefix: 'SOCKET_'
    }
}))

new Vue({
  render: h => h(App), router, store
}).$mount('#app')
