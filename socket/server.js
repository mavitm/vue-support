const app = require("express")();
const server = require('http').Server(app)
const port = 3000;
const io = require("socket.io").listen(server);

io.sockets.on('connection', function (socket) {
    console.log("socket Connected");

    socket.on("disconnect", function () {
        console.log("socket Disconnected");
    });

    socket.on("message", function (data) {
        socket.emit("show-message", 'senin mesajın:'+data);
        socket.broadcast.emit("show-message", data);
    });

    socket.on("idCneection", function (jsn) {
        io.to(jsn.id).emit("privatemessage", jsn.message);
        io.to(jsn.selfid).emit("privatemessage", "sen:"+jsn.message);
        console.log(jsn);
    });

    socket.on("clientmessage", function(clientmessage){
        console.log(clientmessage);
        io.to(clientmessage[3]).emit("setRemoteMessage", ['1','mesaj alindi', new Date()]);
    });

});

app.get("/", function (req, res) {
    console.log("home page");
    res.send("main");
} );

app.get("/person", (req, res) =>  {
    console.log("rerson page");
    res.status(200).set('Content-Type', 'text/html');
    res.sendFile(
        __dirname+"/server.html",
    );
} );

server.listen(port, () => console.log(`Example app listening on port http://127.0.0.1:${port}!`))